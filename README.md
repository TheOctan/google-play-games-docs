# Platform Requirements
## [Support x86 ABI Architecture](https://developer.android.com/games/playgames/pc-compatibility#x86-requirement)

1) Go to Player Settings > Other Settings > Configuration > Scripting Backend and select IL2CPP from the dropdown menu to enable the IL2CPP Scripting Backend.

2) Enable x86 Android targets for your version of Unity:

* Unity 2018 and earlier: go to Player Settings > Other Settings > Target Architecture, and select the x86 checkbox.

* Unity 2019 Long Term Support (LTS) release and later: go to Player Settings > Other Settings > Target Architectures and enable both x86 (Chrome OS) and x86-64 (Chrome OS).

To maximize game engine support, we recommend enabling both x86 and x86-64 support, or only x86-64 so you're not limited by a 32 bit memory space.

# PC Compatibility
## [Support mouse and keyboard](https://developer.android.com/games/playgames/input#input-support)

### Best practices
We recommend the following best practices for designing and building player interactions.

* All targets should be clickable with a mouse.
* All scrollable surfaces scroll on mouse wheel events.
* Highlight clickable surfaces when hovered, and use your best judgment to improve UI discovery without overwhelming the user.
* Provide hotkeys for users to quickly bring up controls.
* Replace touch-based controls (for example, thumb joysticks or on-screen buttons) with mouse-based controls and hotkeys.
* For actions in your mobile game that require multi-touch gestures, ensure the same actions are supported with a keyboard or mouse control (for example, using the scroll wheel of the mouse to replace a two-finger pinch).

### Recommended input mappings
> The following list includes actions many games have in common, and the typical implementation developers use on Google Play Games:

Use the enter key to send messages or submit text in text entry fields.
* Menus and dialogs should be cancellable with the escape key.
* Use the enter key to progress through story elements and dialog boxes.
* Use the scroll wheel to scroll text vertically.
* Use the scroll wheel to zoom in or out, especially if you use a two-finger pinch in your mobile build.
* Use W, A, S, and D navigate around a map that you'd normally use a click and drag motion on.

## [Input SDK](https://developer.android.com/games/playgames/input-sdk)
The Input SDK provides a unified interface from which players can discover the mouse and keyboard bindings for any game they wish to play on Google Play Games. At any point during gameplay, a player can summon the Input SDK overlay in a Google Play Games title as seen in this screenshot:
![image](Images/input-sdk-overlay.png)

This SDK is required for Google Play Games because Android mobile games are designed around a touchscreen for player input. When developing for PCs, games need to support a mouse and keyboard instead. You should only enable this SDK on Google Play Games.

### Integration guide
[Get started with the Input SDK](https://developer.android.com/games/playgames/input-sdk-start)

## [Unsupported Android features and permissions](https://developer.android.com/games/playgames/pc-compatibility#unsupported-android-features)

On a PC, some Android features available on a mobile phone or tablet are inaccessible. This includes hardware features such as the camera and other features like a user's location. As a result, your game must not rely on unsupported Android features or permissions. On PC, if your game requests access to an unsupported permission, the request automatically fails.

To make your game compatible with PC, the following changes are required:

* In your app manifest, add android:required="false" to the <uses-feature> declaration for all features that Google Play Games does not support. This applies to only the features already declared in your app manifest.
* Disable features from your game that depend on unsupported hardware and software features on PC. If you are using the same APK as your mobile game, then you can conditionally disable them for PC.
* Disable requests for unsupported Android permissions from your game, and features that depend on these permissions. If you are using the same APK as your mobile game, you can conditionally make these changes for PC, and you should not make any changes to the permissions declared in the manifest.

## [Disable unsupported Google Play Service APIs](https://developer.android.com/games/playgames/pc-compatibility#unsupported-google-apis)
### Supported Modules

These modules are currently available and will be supported by Google Play Games in the future:

* Google Sign-In (not including account transfer, SmartLock, SMS verification, Password complexity calculation)
* Cronet
* Google Play Games Services
* Tasks
* Vision
* Google Pay

### Limited Support
The following modules are partially functional. We will do our best to support them on Google Play Games, but we cannot guarantee their functionality.

* Google AdMob (Mobile ads, Ad ID)
* Google Cloud Messaging (Deprecated, use Firebase Cloud Messaging)
* Firebase Authentication (Phone number auth does not work)
* Firebase Cloud Messaging
* Firebase Common Libraries
* Firebase ML
* Firebase Remote Config
* Firebase Analytics

### Not Supported

These modules are not supported in Google Play Games, but do not cause issues in Google Play Games when they fail:

* Google Analytics (Deprecated, use Firebase Analytics)
* Google Cast
* Awareness API
* Drive (Deprecated, will be removed soon)
* FIDO
* Firebase Realtime Database
* Firestore
* Firebase A/B Testing
* Google Fit
* Address API
* Instant Apps API
* Location API
* Google Maps SDK
* Nearby
* Panorama
* Places
* Google+
* SafetyNet (Deprecated, please fill the interest form for opting-in to the upcoming Play Integrity API)
* Google Tag Manager
* Wear OS

### Broken

You should not use these moodules because they can cause unexpected behavior on Google Play Games.

* Firebase Dynamic Links

## [Enable scoped storage](https://developer.android.com/games/playgames/pc-compatibility#scoped-storage)

This section applies if your game read/writes to external storage. Scope storage enforcement is required as an alternative way to read and write to storage. Doing this removes the need to prompt the player for these sensitive permissions:

> android.permission.READ_EXTERNAL_STORAGE
>
> android.permission.WRITE_EXTERNAL_STORAGE

## [Disable mobile ads in the PC version of your game](https://developer.android.com/games/playgames/pc-compatibility#disable-mobile-ads)

To optimize your game for PC, you must disable mobile ads on your PC version, since they are incompatible with desktop and laptop PCs. This includes all ad units provided by mobile ad SDKs. You are not required to make any changes to ads in the mobile version of your game. We recommend using feature flags to limit changes only to the PC version of your game. You can still keep the mobile ads on Chrome OS.

When removing mobile ads, the following steps are required:

* Disable all mobile ad units, including banners, interstitials, and rewarded ads.
* Disable features from your game that depend on rewarded ads. For example, a feature that lets users watch a video ad to earn in-game currency could have the button deactivated or removed completely.э

# Display / Visual Assets
## Use high resolution assets and textures on PC
## Support PC-specific aspect ratios
## Scale UI elements appropriately for a larger screen
## Render at 60Hz
## Audio playback is smooth and in sync

# Cross-Platform Play
## Automatically sign-in with Google Play Games Services on mobile and PC

## Automatically sync save games between mobile and PC